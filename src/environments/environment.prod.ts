export const environment = {
  production: true,
  clave : "leoncrop",
  imageInvalid : "https://upload.wikimedia.org/wikipedia/commons/thumb/1/12/User_icon_2.svg/220px-User_icon_2.svg.png",
  endPoint : 'https://jslogic.herokuapp.com/api/'
};
