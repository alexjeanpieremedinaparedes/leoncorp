export interface IAuth {
    status:  number;
    message: string;
    result:  AuthResult;
}

export interface AuthResult {
    token:             string;
    Nombre:            string;
    IDNombres:         string;
    IDApellidos:       string;
    RazonSocial:       string;
    Sede:              string;
    Ciudad:            string;
    IDSede:            number;
    DNI:               string;
    IDUsuario:         number;
    Administrador:     boolean;
    IDEmpresa:         number;
    Director:          string;
    ApellidosDirector: string;
    IDDirector:        number;
    Logo:              string;
    Foto:              string;
}
