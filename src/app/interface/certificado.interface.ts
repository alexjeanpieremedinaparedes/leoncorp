
export interface ICertificado {
    IDTipoCertificado:       number;
    Alias:                   string;
    NombreCertificado:       string;
    Imagen:                  string;
    Resolucion:              string;
    CantHoras:               number;
    UtilizaFoto:             boolean;
    Ruta:                    string;
    IDCertificadoAutorizado: number;
}


export interface ICertReporte {
    Serial: string;
    Numero:                  string;
    IDCliente:               number;
    NumeroIdentificacion:    string;
    Cliente:                 string;
    TipoCertificado:         string;
    Certificado:             string;
    IDTipoCertificado:       number;
    Resolucion:              string;
    CantHoras:               number;
    UtilizaFoto:             boolean;
    IDCertificadoAutorizado: number;
    IDCertificado:           number;
    IDSede:                  number;
    Fecha:                   string;
    Mes:                     string;
    Reponsable:              string;
    Sede:                    string;
    IDDirector:              number;
    Director:                string;
}
