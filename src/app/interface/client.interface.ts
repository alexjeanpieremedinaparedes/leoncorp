
export interface IclienteBase {
    status  :  number;
    message : string;
    result  :  IclientResultBase[];
}

export interface IclientResultBase {
    IDCliente   :     number;
    IDDocumento :     number;
    Documento   :     string;
    NumeroIdentificacion: string;
    Nombres     :     string;
    Apellidos   :     string;
    Direccion   :     string;
    Telefono1   :     string;
    Telefono2   :     string;
    Correo      :     string;
    FechaNacimiento : string;
    IDDepartamento  : number,
    Departamento    : string;
    Revalidar       : boolean;
}
