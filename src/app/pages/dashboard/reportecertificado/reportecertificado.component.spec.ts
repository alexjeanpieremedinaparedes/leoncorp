import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportecertificadoComponent } from './reportecertificado.component';

describe('ReportecertificadoComponent', () => {
  let component: ReportecertificadoComponent;
  let fixture: ComponentFixture<ReportecertificadoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReportecertificadoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportecertificadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
