import {AfterViewInit, Component, ViewChild} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { NgxSpinnerService } from 'ngx-spinner';
import { ICertReporte } from 'src/app/interface/certificado.interface';
import { CertificadoService } from 'src/app/service/certificado.service';
import { ClientService } from 'src/app/service/client.service';
import { FunctionService } from 'src/app/service/function.service';
import { ValidatorService } from 'src/app/service/validator.service';
import { EditreportecertificadoComponent } from '../../modal/editreportecertificado/editreportecertificado.component';


@Component({
  selector: 'app-reportecertificado',
  templateUrl: './reportecertificado.component.html',
  styleUrls: ['./reportecertificado.component.css']
})
export class ReportecertificadoComponent implements AfterViewInit {

  form : FormGroup;

  success  : boolean;
  error    : boolean;
  cnn_expi : boolean;
  message  : string;

  _login   = JSON.parse(localStorage.getItem('_loginleon'));
  listSedes : any[] = [];
  displayedColumns: string[] = ['numero', 'documento', 'cliente', 'tipoc', 'resolucion', 'horas' , 'fecha', 'director',  'editar' , 'quitar' ];
  dataSource = new MatTableDataSource<ICertReporte>([]);

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  constructor(
    public modals:MatDialog,
    private sfn   : FunctionService,
    private scert : CertificadoService,
    private sclient : ClientService,
    public svalidator : ValidatorService,
    private fb : FormBuilder,
    private spinner : NgxSpinnerService
  ) {

    this.getSedes();
    this.create_form();
    this.changeBusqueda("1");
  }

  editreportecert( item : ICertReporte ){
    this.modals.open(EditreportecertificadoComponent, {
      data : item,
      disableClose : true
    }).afterClosed().subscribe( response =>{

      if( response.exito ){

        this.listReporte();
        this.success = true;
        this.message = 'Certificado editado con exito !!!';
        this.hiddenExito();
      }
    })
  }

  get dniInvalid(){
    return this.svalidator.control_invalid('DNI', this.form);
  }

  get fechaiInvalid(){
    return this.svalidator.control_invalid('FechaInicial', this.form);
  }

  get fechafInvalid(){
    return this.svalidator.control_invalid('FechaFinal', this.form);
  }

  get sedeInvalid(){
    return this.svalidator.control_invalid('IDSede', this.form);
  }

  create_form(){

    this.form = this.fb.group({

      TipoBusqueda : [ '1', [ Validators.required ] ],
      DNI          : [ '' ],
      FechaInicial : [ new Date() ],
      FechaFinal   : [ new Date() ],
      IDSede       : [ this._login.IDSede, Validators.required ],

      allSedes     : [ false ]
    });
  }

  getSedes(){

    this.spinner.show();
    this.sclient.getSedes().subscribe( response =>{

      this.listSedes = response;
      this.spinner.hide();
    }, (e) => this.exeception(e) );
  }

  listReporte(){

    if(this.form.invalid){
      return this.svalidator.Empty_data(this.form);
    }

    this.spinner.show();
    this.initialize();
    const body = {
      ... this.form.value
    };

    delete body.allSedes;  
    body.FechaInicial = this.sfn.convert_fecha(body.FechaInicial);
    body.FechaFinal = this.sfn.convert_fecha(body.FechaFinal);

    this.scert.getReporteCertificado( body ).subscribe( response =>{

      this.dataSource = new MatTableDataSource<ICertReporte>(response);
      this.dataSource.paginator = this.paginator;
      this.spinner.hide();
    }, (e) => this.exeception(e) );

  }

  changeBusqueda( $event:string ){

    this.clearValidators();
    if( $event === "1" ) this.svalidator.addValidator('DNI', Validators.required, this.form);
    else if ( $event === "2" ){

      this.svalidator.addValidator('DNI', Validators.required, this.form);
      this.svalidator.addValidator('FechaInicial', Validators.required, this.form);
      this.svalidator.addValidator('FechaFinal', Validators.required, this.form);
    }
    else{

      this.svalidator.addValidator('FechaInicial', Validators.required, this.form);
      this.svalidator.addValidator('FechaFinal', Validators.required, this.form);
    }

  }

  exportAsExcel() {

    if( this.dataSource.data.length > 0 ){

      this.spinner.show();
      const name_file = 'Reporte de certificados';
      const jsonString = JSON.stringify(this.dataSource.data);
      const json = JSON.parse(jsonString);

      json.forEach((el:ICertReporte) => {

        delete el.IDCertificado;
        delete el.IDCertificadoAutorizado;
        delete el.IDCliente
        delete el.IDDirector
        delete el.IDSede
        delete el.IDTipoCertificado 
      });

      this.sfn.exportToExcel(json, name_file)
      this.spinner.hide();
    }
    else {
      this.error = true;
      this.message = 'No hay datos para exportar';
    }
  }

  print( item :ICertReporte ){
    
    this.spinner.show();
    this.initialize();
    const body = {
      Numero : item.Numero
    };

    this.scert.printCertificado(body).subscribe( response =>{

      const exito = response['message'] === 'exito';
      if( exito ){
        const bs64 = response['result'];
        this.sfn.printDocument(bs64);
      }

      this.spinner.hide();
    }, (e) => this.exeception(e) );
  }

  resetSedes(){

    const allsedes = this.form.value.allSedes;
    if(allsedes){
      
      this.form.patchValue({
        IDSede : this._login.IDSede,
        TipoBusqueda : '4'
      });
      
      this.changeBusqueda("4");
    }
    else{

      this.form.patchValue({
        IDSede : this._login.IDSede,
        TipoBusqueda : '1'
      });
      
      this.changeBusqueda("1");
    }

  }

  clearValidators(){

    this.svalidator.addValidator('DNI', null, this.form);
    this.svalidator.addValidator('FechaInicial', null, this.form);
    this.svalidator.addValidator('FechaFinal', null, this.form);

    if( this.form.value.TipoBusqueda === '3' || this.form.value.TipoBusqueda === '4'  ){
      
      this.form.patchValue({
        DNI          : '',
        FechaInicial : new Date(),
        FechaFinal   : new Date(),
      });
    }

    const allSedes = this.form.value.TipoBusqueda === '4';
    if(!allSedes){
      this.form.patchValue({ allSedes : false });
    }
  }

  exeception( error:any ){
    
    this.error = true;
    const cnn_expi = error.error === 'Unauthorized';
    this.cnn_expi = cnn_expi;
    this.message = cnn_expi ? 'conexion expirada, vuelva a iniciar sesion' : error.error.message ?? 'Sin conexion al servidor';
    this.spinner.hide();
  }

  initialize() {

    this.success = false;
    this.cnn_expi = false;
    this.error = false;
    this.message = null;
  }

  hiddenExito() {

    setTimeout(() => {
      this.success = false;
    }, 2000);

  }

}