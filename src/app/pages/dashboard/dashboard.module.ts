import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegistrocertificadoComponent } from './registrocertificado/registrocertificado.component';
import { ReportecertificadoComponent } from './reportecertificado/reportecertificado.component';
import { DashboardComponent } from './dashboard.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { MaterialModule } from '../../material-module';
import { NuevoclienteComponent } from '../modal/nuevocliente/nuevocliente.component';
import { ComponentsModule } from '../../components/components.module';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ReactiveFormsModule } from '@angular/forms';
import { ClientesComponent } from './clientes/clientes.component';


@NgModule({
  
  declarations: [
    RegistrocertificadoComponent,
    ReportecertificadoComponent,
    DashboardComponent,
    NuevoclienteComponent,
    ClientesComponent
  ],

  exports: [
    RegistrocertificadoComponent,
    ReportecertificadoComponent,
    DashboardComponent
  ],

  imports: [
    CommonModule, 
    RouterModule,
    SharedModule, 
    AppRoutingModule, 
    MaterialModule,
    ComponentsModule, 
    NgxSpinnerModule,
    ReactiveFormsModule
  ]

})
export class DashboardModule { }
