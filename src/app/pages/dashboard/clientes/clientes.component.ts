import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { NuevoclienteComponent } from '../../modal/nuevocliente/nuevocliente.component';
import { RegistrarnuevocliComponent } from '../../modal/registrarnuevocli/registrarnuevocli.component';



@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styles: [
  ]
})
export class ClientesComponent implements OnInit {

  newClient : boolean;
  form : FormGroup;

  displayedColumns: string[] = ['tipodoc', 'numerod', 'nombre', 'apellidos', 'departamento', 'direccion' , 'telefono', 'correo' , 'fecha',  'editar' , 'quitar' ];
  dataSource = ELEMENT_DATA;

  constructor( public modal:MatDialog) { }

  ngOnInit(): void {
  }

  nuevocliente(){
    this.modal.open(RegistrarnuevocliComponent)
  }

  SaveClient(){

  }

}

export interface PeriodicElement {  
  Documento  :string ;
  NumeroIdentificacion:string;
  Nombres: string;
  Apellidos: string;
  Departamento: string;
  Direccion :string;
  Telefono1: string;
  Correo: string;
  FechaNacimiento: string;

}

const ELEMENT_DATA: PeriodicElement[] = [
  {Documento: "dni", NumeroIdentificacion: '125478', Nombres: "Bernardo", Apellidos: 'Peña', Departamento:"LA Libertad", Direccion:"22 de febrero", Telefono1:"7658422", Correo:"ejemplo@gmail.com", FechaNacimiento:"2/05/21"},
  
];


