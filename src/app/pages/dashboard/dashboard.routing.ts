import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { DashboardComponent } from './dashboard.component';
import { RegistrocertificadoComponent } from './registrocertificado/registrocertificado.component';
import { ReportecertificadoComponent } from './reportecertificado/reportecertificado.component';
import { AuthGuard } from 'src/app/guard/auth.guard';
import { ClientesComponent } from './clientes/clientes.component';



const routes: Routes = [

    { path: 'dashboard', component: DashboardComponent,

    children: [

      { path: 'registrocertificado', component: RegistrocertificadoComponent, canActivate : [ AuthGuard ] },

      { path: 'reportecertificado', component: ReportecertificadoComponent, canActivate : [ AuthGuard ] },

      { path: 'clientes', component: ClientesComponent, canActivate : [ AuthGuard ] },

      { path: '**', redirectTo: 'registrocertificado'},
      
    ]
 },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DashboardRoutingModule {}
