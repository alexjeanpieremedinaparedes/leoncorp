import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrocertificadoComponent } from './registrocertificado.component';

describe('RegistrocertificadoComponent', () => {
  let component: RegistrocertificadoComponent;
  let fixture: ComponentFixture<RegistrocertificadoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistrocertificadoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrocertificadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
