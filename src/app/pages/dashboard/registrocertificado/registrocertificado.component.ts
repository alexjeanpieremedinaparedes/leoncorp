
import {AfterViewInit, Component, ViewChild} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { NgxSpinnerService } from 'ngx-spinner';
import { ICertificado, ICertReporte } from 'src/app/interface/certificado.interface';
import { CertificadoService } from 'src/app/service/certificado.service';
import { ClientService } from 'src/app/service/client.service';
import { FunctionService } from 'src/app/service/function.service';
import { ValidatorService } from 'src/app/service/validator.service';
import { NuevoclienteComponent } from '../../modal/nuevocliente/nuevocliente.component';
import { VerCertificadoComponent } from '../../modal/ver-certificado/ver-certificado.component';

@Component({
  selector: 'app-registrocertificado',
  templateUrl: './registrocertificado.component.html',
  styleUrls: ['./registrocertificado.component.css']
})
export class RegistrocertificadoComponent implements AfterViewInit {

  SaveCompleted : boolean;
  delete   : boolean;
  success  : boolean;
  error    : boolean;
  cnn_expi : boolean;
  message  : string;
  indexItem: number;
  
  certificados :boolean;
  form  : FormGroup;
  _login = JSON.parse(localStorage.getItem('_loginleon'));

  listCertificados : ICertificado[];
  
  displayedColumns: string[] = ['numero', 'neditificacion', 'cliente', 'tipoc', 'resolucion', 'horas'  ,  'ver' , 'quitar' ];
  dataSource = new MatTableDataSource([]);
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(
    public modals     : MatDialog,
    public  sfn       : FunctionService,
    public  svalidator: ValidatorService,
    private sclient   : ClientService,
    private scert     : CertificadoService,
    private fb        : FormBuilder,
    private spinner   : NgxSpinnerService
    ) {
    this.getCertificados();
    this.create_form();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit() { }

  verCertificado( item:ICertReporte ){

    if(item.Serial === '--'){

      this.error = true;
      this.message = 'la fila no tiene serial';
      return;
    }

    this.modals.open(VerCertificadoComponent, {
      data : item,
      disableClose : true
    })
  }

  getCertificados(){

    this.spinner.show();
    const body = {
      IDSede  : this._login.IDSede
    };

    this.scert.getCertificadoAutorizado( body).subscribe( response =>{

      this.listCertificados = response;
      this.spinner.hide();
    }, (e) => this.exeception(e) );
  }

  get dniInvalid(){
    return this.svalidator.control_invalid('Filtro', this.form);
  }

  nuevocliente( validarReniec:boolean = true ){

    const body = {
      ... this.form.value
    };

    body.validarReniec = validarReniec

    this.modals.open(NuevoclienteComponent,{ 
      data : body,
      disableClose : true
    }).afterClosed().subscribe( result =>{

      if( result.exito ){

        const operacion = result.nuevo ? "registrado" : 'editado';
        const body = result.body;
        const nombres = `${body.Nombres} ${body.Apellidos}`
        this.form.patchValue({
          nombres : nombres,
          IDCliente : body.IDCliente,
          Filtro    : body.NumeroIdentificacion
        });
        this.certificados = true;
        this.success = true;
        this.message = `Cliente ${operacion} con éxito!!!`
        this.hiddenExito();
      }

    });
  }

  showModalDelete( index : number ){

    this.indexItem = index;
    this.delete = true;
    this.message = '¿ Desea quitar el item ?';
  }

  quitarItem( $event ){

    if( $event ){

      this.dataSource.data.splice(this.indexItem, 1);
      this.dataSource.paginator = this.paginator;
    }

  }

  create_form(){

    this.form = this.fb.group({
      Filtro  : [ '', [ Validators.required, Validators.pattern('^[0-9]{8}$') ] ], //Validators.pattern('^[0-9]{8}$')
      IDSede  : [ this._login.IDSede, [ Validators.required ] ],
      nombres : [ '' ],
      IDCliente : [ 0 ]
    });
  }

  search(){

    if(this.form.invalid){
      return this.svalidator.Empty_data(this.form);
    }
    
    this.spinner.show();
    this.initialize();
    const body = {
      ... this.form.value
    }

    delete body.nombres;
    delete body.IDCliente;

    this.sclient.getClientBase(body).subscribe( response =>{

      const exito = response.message === 'exito';
      
      if( exito ){
        const result = response.result;
        if( result.length > 0 ){
          
          this.form.patchValue({
            nombres : `${result[0].Nombres} ${result[0].Apellidos}`,
            IDCliente : result[0].IDCliente
          })
          this.certificados = true;
        }
        else{
          this.nuevocliente();
        }
      }
      
      this.spinner.hide();
    }, (e) => this.exeception(e) )

  }

  ir_certificado( item : any ){

    let array = [];
    const documento = this.form.value.Filtro;
    const cliente = this.form.value.nombres;
    const idcliente = this.form.value.IDCliente;
    const idsede = this.form.value.IDSede;

    if( !documento ){

      this.error = true;
      this.message = 'Ingrese número de documento de identidad';
      return;
    }

    const existe = this.dataSource.data.find( x=>x.NombreCertificado === item.NombreCertificado && x.Cliente === cliente );
    if(existe){

      this.error = true;
      this.message = 'El cliente ya tiene el mismo certificado asignado';
      this.certificados = false;
      return;
    }

    if( this.SaveCompleted ){

      this.error = true;
      this.message = 'Imprima certificados para finalizar el proceso';
      this.certificados = false;
      return;
    }

    let arrayString = JSON.stringify(this.dataSource.data);
    item.Documento = documento;
    item.Cliente = cliente;
    item.IDCliente = idcliente;
    item.IDSede = idsede;
    item.Serial = '--';
    item.iid = this.dataSource.data.length;

    array = JSON.parse(arrayString);
    array.push(item);

    this.dataSource = new MatTableDataSource(array);
    this.dataSource.paginator = this.paginator;
    this.certificados = false;
  }

  saveCertificado(){

    if ( this.dataSource.data.length <= 0 ){

      this.error = true;
      this.message = 'no tiene certificados en la lista para guardar';
      return;
    }

    this.SaveCompleted = false;
    this.dataSource.data.forEach( (el, i) =>{
      const body = {
        IDCliente : el.IDCliente,
        dni       : el.Documento,
        IDSede    : el.IDSede,
        IDUsuario : this._login.IDUsuario,
        Resolucion: el.Resolucion,
        IDDirector: this._login.IDDirector,
        IDTipoCertificado: el.IDTipoCertificado,
        Horas     : el.CantHoras 
      };

      this.spinner.show();
      this.scert.SaveCertificado( body ).subscribe( response =>{
  
        const exito = response['message'] === 'exito';
        if(exito){
          
          const serial = response['result']['result'][0]['Serial'];
          this.dataSource.data[i].Serial = serial;
          this.SaveCompleted = true;
        }
  
        this.spinner.hide();
      }, (e)=> this.exeception(e) );
    })
  }

  printMassive(){

    if ( this.dataSource.data.length == 0 ){

      this.error = true;
      this.message = 'No tiene certificados en la lista para imprimir';
      return;
    }

    let Numeros=[];

    this.dataSource.data.forEach( (el, i) =>{
      Numeros.push(el.Serial);
    });

    this.spinner.show();
    const body = {
      Numeros : Numeros //Cambiar por un arreglo de seriales
    };

    this.scert.printCertificadoGrupal(body).subscribe( response =>{
      const exito = response['message'] === 'exito';
      if( exito ){
        const bs64 = response['result'];
        this.sfn.printDocument(bs64);

        this.SaveCompleted = false;
        this.dataSource = new MatTableDataSource([]);
      }

      this.spinner.hide();
    }, (e) => this.exeception(e) );
  }

  exeception( error:any ){
    
    this.error = true;
    const cnn_expi = error.error === 'Unauthorized';
    this.cnn_expi = cnn_expi;
    this.message = cnn_expi ? 'conexion expirada, vuelva a iniciar sesion' : error.error.message ?? 'Sin conexion al servidor';
    this.spinner.hide();
  }

  initialize() {
    this.delete = false;
    this.success = false;
    this.cnn_expi = false;
    this.error = false;
    this.certificados = false;
    this.message = null;

    this.form.patchValue({ nombres : '' })
  }

  hiddenExito() {

    setTimeout(() => {
      this.success = false;
    }, 2000);

  }

  validatedni( $event:any, limite:number ){

    let inputValue = (document.getElementById('dniSelect') as HTMLInputElement).value;
    const select = document.getSelection().toString() === inputValue;
    if( select ){
      (document.getElementById('dniSelect') as HTMLInputElement).value = '';
    }
    return this.svalidator.validateKey($event, limite);


  }
}