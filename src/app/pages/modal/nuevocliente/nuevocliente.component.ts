
import {AfterViewInit, Component, Inject, ViewChild} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { NgxSpinnerService } from 'ngx-spinner';
import { IclientResultBase } from 'src/app/interface/client.interface';
import { ClientService } from 'src/app/service/client.service';
import { FunctionService } from 'src/app/service/function.service';
import { ValidatorService } from 'src/app/service/validator.service';
import { RegistrocertificadoComponent } from '../../dashboard/registrocertificado/registrocertificado.component';
import * as moment from 'moment';

@Component({
  selector: 'app-nuevocliente',
  templateUrl: './nuevocliente.component.html',
  styleUrls: ['./nuevocliente.component.css']
})
export class NuevoclienteComponent implements AfterViewInit {

  form : FormGroup;
  idClientedelete : number;

  delete   : boolean;
  success  : boolean;
  error    : boolean;
  cnn_expi : boolean;
  message  : string;

  listDepartamento : any[] = [];
  listDocument     : any[] = [];
  listSedes        : any[] = [];
  listClientXSede  : IclientResultBase[];

  _login = JSON.parse(localStorage.getItem('_loginleon'));

  displayedColumns: string[] = ['tipodoc', 'numerod', 'nombre', 'apellidos', 'departamento', 'direccion' , 'telefono', 'correo' , 'fecha',  'editar' , 'quitar' ];
  dataSource = new MatTableDataSource<IclientResultBase>([]);

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;
  
  constructor(
    public modals:MatDialog,
    private fb : FormBuilder,
    public svalidator : ValidatorService,
    private sfn       : FunctionService,
    private sclient   : ClientService,
    private spinner   : NgxSpinnerService,
    private dialogRef : MatDialogRef<RegistrocertificadoComponent>,
    @Inject(MAT_DIALOG_DATA) private data : any
    ) {
      
    this.getClientesXsede();
    this.getDepartamento();
    this.getTypeDocument();
    this.getSedes();
    this.create_form();
    if(data.validarReniec) this.getReniec();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  get sedeInvalid(){
    return this.svalidator.control_invalid('IDSede', this.form);
  }

  get documentInvalid(){
    return this.svalidator.control_invalid('IDDocumento', this.form);
  }

  get numerodocInvalid(){
    return this.svalidator.control_invalid('NumeroIdentificacion', this.form);
  }

  get nombresInvalid(){
    return this.svalidator.control_invalid('Nombres', this.form);
  }

  get apellidosInvalid(){
    return this.svalidator.control_invalid('Apellidos', this.form);
  }

  get telefonoInvalid(){
    return this.svalidator.control_invalid('Telefono1', this.form);
  }

  get departamentoInvalid(){
    return this.svalidator.control_invalid('IDDepartamento', this.form);
  }

  get fechaNaciInvalid(){
    return this.svalidator.control_invalid('FechaNacimiento', this.form);
  }

  get correoInvalid(){
    return this.svalidator.control_invalid('Correo', this.form);
  }


  create_form(){

    const dni = this.data.Filtro;
    const reporte = this.data.reporte ?? false;
    const idsede = reporte ? this.data.IDSede : this._login.IDSede;

    this.form = this.fb.group({
      IDCliente            : [ 0 ],
      IDDocumento          : [ 1, [ Validators.required ] ],
      NumeroIdentificacion : [ dni, [ Validators.required ] ],
      Nombres              : [ '', [ Validators.required ] ],
      Apellidos            : [ '', [ Validators.required ] ],
      Direccion            : [ '' ],
      Telefono1            : [ '', [ Validators.required ] ],
      Telefono2            : [ '' ],
      Correo               : [ '', [ Validators.pattern('[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,3}$') ] ],
      FechaNacimiento      : [ new Date(), [ Validators.required ] ],
      Guardar              : [ true ],
      IDSede               : [ idsede, [ Validators.required ] ],
      IDDepartamento       : [ '', [ Validators.required ] ],
      Revalidar            : [ false, [ Validators.required ] ]    
    });

  }

  getTypeDocument(){

    this.spinner.show();
    this.initialize();
    this.sclient.getTypeDocument().subscribe( (response : any[]) =>{
      
      this.listDocument = response;
      this.spinner.hide();
    }, (e) => this.exeception(e) );
  }

  getSedes(){

    this.spinner.show();
    this.sclient.getSedes().subscribe( response =>{

      this.listSedes = response;
      this.spinner.hide();
    }, (e) => this.exeception(e) );
  }

  getDepartamento(){

    this.spinner.show();
    this.sclient.getDepartamento().subscribe( response =>{

      this.listDepartamento = response;
      this.spinner.hide();
    }, (e) => this.exeception(e) );
  }

  getReniec(){

    this.spinner.show();
    this.form.patchValue({Nombres : '', Apellidos : ''});
    this.initialize();
    const dni = this.form.value.NumeroIdentificacion;
    if(!dni){

      this.error = true;
      this.message = 'Ingrese dni para continuar';
      return;
    }

    const body = {
      dni : dni
    };

    this.sclient.getclientReniec(body).subscribe( response =>{

      const exito = response['message'] === 'Finalizado';
      if( exito ){

        const result = response['result'][0];
        const nombres = result['nombres'];
        const apellidos = `${result['paterno']} ${result['materno']}`;

        this.form.patchValue({

          Nombres : nombres,
          Apellidos : apellidos
        });
      }

      this.spinner.hide();

    }, (e) => this.exeception(e) );

  }

  getClientesXsede(){

    this.spinner.show();

    const body = {
      Filtro : '',
      IDSede : this._login.IDSede
    };

    this.sclient.getClientesXsede( body ).subscribe( response =>{

      this.dataSource = new MatTableDataSource(response);
      this.listClientXSede = response;
      this.dataSource.paginator = this.paginator;
      this.spinner.hide();
    }, (e) => this.exeception(e) );

  }

  filterCliente( text:string ){

    this.dataSource = new MatTableDataSource( this.listClientXSede  );
    this.dataSource = new MatTableDataSource( this.sfn.search( this.dataSource.data, 'NumeroIdentificacion', text ) );

    const filterNotExito = this.dataSource.filteredData.length === 0;
    if(filterNotExito){

      this.dataSource = new MatTableDataSource( this.listClientXSede  );
      this.dataSource = new MatTableDataSource( this.sfn.search( this.dataSource.data, 'Nombres', text ) );
    }

    this.dataSource.paginator = this.paginator;
  }

  save(){

    if(this.form.invalid){
      return this.svalidator.Empty_data(this.form);
    }

    this.spinner.show();
    const body = {
      ... this.form.value
    };

    body.FechaNacimiento = this.sfn.convert_fecha(body.FechaNacimiento);
    this.sclient.saveClient( body ).subscribe( response =>{

      const exito = response['message'] === 'exito';
      if( exito ){

        this.success = true;
        const result = response['result'][0];
        const nuevo = this.form.value.Guardar;
        this.dialogRef.close({ exito : true, nuevo : nuevo ,body : result });
      }
      else{
        this.error = true;
        this.message = response['message'];
      }
      this.spinner.hide();
    }, (e) => this.exeception(e) );

  }

  loadEdit( item : IclientResultBase){

    const fecha = moment(item.FechaNacimiento, 'DD/MM/YYYY').toDate();
    this.form.patchValue({

      IDCliente            : item.IDCliente,
      IDDocumento          : item.IDDocumento,
      NumeroIdentificacion : item.NumeroIdentificacion,
      Nombres              : item.Nombres,
      Apellidos            : item.Apellidos,
      Direccion            : item.Direccion,
      Telefono1            : item.Telefono1,
      Correo               : item.Correo,
      FechaNacimiento      : fecha,
      Guardar              : false,
      IDSede               : this._login.IDSede,
      IDDepartamento       : item.IDDepartamento,
      Revalidar            : item.Revalidar ?? false,
    });
  }

  showDelete( idCliente:number ){
    this.idClientedelete = idCliente;
    this.delete = true;
    this.message = '¿ Desea eliminar el cliente ?'
  }

  deleteClient( $event:any ){

    if(!$event) return;

    this.spinner.show();
    const body = {
      IDCliente : this.idClientedelete
    };

    this.sclient.deleteClient(body).subscribe( response =>{

      const exito = response['message'] === 'exito';
      if(exito){

        this.success = true;
        this.message = 'Cliente eliminado con éxito!!!'
        this.getClientesXsede();
        this.hiddenExito();
      }

      this.spinner.hide();
    }, (e) => this.exeception(e) );

  }

  changeRevalidacion(){

    this.form.patchValue({ FechaNacimiento : new Date()  })
  }

  exeception( error:any ){
    
    this.error = true;
    const cnn_expi = error.error === 'Unauthorized';
    this.cnn_expi = cnn_expi;
    this.message = cnn_expi ? 'conexion expirada, vuelva a iniciar sesion' : error.error.message ?? 'Sin conexion al servidor';
    this.spinner.hide();
  }

  initialize() {

    this.delete = false;
    this.success = false;
    this.cnn_expi = false;
    this.error = false;
    this.message = null;
    this.idClientedelete = 0;
  }

  hiddenExito() {

    setTimeout(() => {
      this.success = false;
    }, 2000);

  }

}

