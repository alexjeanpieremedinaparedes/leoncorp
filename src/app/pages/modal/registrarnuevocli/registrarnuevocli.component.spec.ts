import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrarnuevocliComponent } from './registrarnuevocli.component';

describe('RegistrarnuevocliComponent', () => {
  let component: RegistrarnuevocliComponent;
  let fixture: ComponentFixture<RegistrarnuevocliComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistrarnuevocliComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrarnuevocliComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
