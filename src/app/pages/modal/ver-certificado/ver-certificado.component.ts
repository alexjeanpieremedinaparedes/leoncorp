import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DomSanitizer } from '@angular/platform-browser';
import { NgxSpinnerService } from 'ngx-spinner';
import { ICertReporte } from 'src/app/interface/certificado.interface';
import { CertificadoService } from 'src/app/service/certificado.service';
import { FunctionService } from 'src/app/service/function.service';

@Component({
  selector: 'app-ver-certificado',
  templateUrl: './ver-certificado.component.html',
  styleUrls: ['./ver-certificado.component.css']
})
export class VerCertificadoComponent implements OnInit {

  bs64File  : any;

  error     : boolean;
  message   : string;
  success   : boolean;
  cnn_expi  : boolean;

  constructor(
    private scert : CertificadoService,
    private sfn   : FunctionService,
    private spinner : NgxSpinnerService,
    private sanitizer: DomSanitizer,
    @Inject( MAT_DIALOG_DATA ) data:ICertReporte ) {

      this.print(data);
    }

  ngOnInit(): void {
  }

  print( item :ICertReporte ){
    
    this.spinner.show();
    this.initialize();
    const body = {
      Numero : item.Serial
    };

    this.scert.printCertificado(body).subscribe( response =>{

      const exito = response['message'] === 'exito';
      if( exito ){
        const bs64 = response['result'];
        this.bs64File = 'data:application/pdf;base64,' + (this.sanitizer.bypassSecurityTrustResourceUrl(bs64) as any).changingThisBreaksApplicationSecurity;  
        top.document.getElementById('ifrm').setAttribute("src", this.bs64File)
      }

      this.spinner.hide();
    }, (e) => this.exeception(e) );
  }

  exeception( error:any ){
    
    this.error = true;
    const cnn_expi = error.error === 'Unauthorized';
    this.cnn_expi = cnn_expi;
    this.message = cnn_expi ? 'conexion expirada, vuelva a iniciar sesion' : error.error.message ?? 'Sin conexion al servidor';
    this.spinner.hide();
  }

  initialize() {

    this.success = false;
    this.cnn_expi = false;
    this.error = false;
    this.message = null;
  }

}
