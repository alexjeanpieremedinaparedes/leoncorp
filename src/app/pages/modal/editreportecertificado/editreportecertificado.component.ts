import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { ICertReporte } from 'src/app/interface/certificado.interface';
import { CertificadoService } from 'src/app/service/certificado.service';
import { ClientService } from 'src/app/service/client.service';
import { ValidatorService } from 'src/app/service/validator.service';
import { ReportecertificadoComponent } from '../../dashboard/reportecertificado/reportecertificado.component';
import { NuevoclienteComponent } from '../nuevocliente/nuevocliente.component';

@Component({
  selector: 'app-editreportecertificado',
  templateUrl: './editreportecertificado.component.html',
  styleUrls: ['./editreportecertificado.component.css']
})
export class EditreportecertificadoComponent implements OnInit {

  form : FormGroup;

  success  : boolean;
  cnn_expi : boolean;
  error    : boolean;
  message  : string;
  listCertificados : any[] = [];

  _login = JSON.parse(localStorage.getItem('_loginleon'));
  
  constructor(
    public modals:MatDialog,
    private sclient : ClientService,
    private scert : CertificadoService,
    public svalidator : ValidatorService,
    private fb : FormBuilder,
    private spinner : NgxSpinnerService,
    private dialogRef : MatDialogRef<ReportecertificadoComponent>,
    @Inject( MAT_DIALOG_DATA ) public data : ICertReporte
  ) {

    this.getCertificados();
    this.create_form();
  }

  ngOnInit(): void {
  }

  get dniInvalid(){
    return this.svalidator.control_invalid('NumeroIdentificacion', this.form);
  }

  get clienteInvalid(){
    return this.svalidator.control_invalid('cliente', this.form);
  }

  get certificadoInvalid(){
    return this.svalidator.control_invalid('IDCertificadoAutorizado', this.form);
  }

  create_form(){
    
    this.form = this.fb.group({
      IDCliente               : [ this.data.IDCliente, [ Validators.required ] ],
      NumeroIdentificacion    : [ this.data.NumeroIdentificacion, [ Validators.required ] ],
      IDCertificadoAutorizado : [ this.data.IDCertificadoAutorizado, [ Validators.required ] ],
      IDCertificado           : [ this.data.IDCertificado, [ Validators.required ] ],
      IDUsuario               : [ this._login.IDUsuario, [ Validators.required ] ],
      cliente                 : [ this.data.Cliente, [ Validators.required ] ],
    });
  }

  search(){

    const dni = this.form.value.NumeroIdentificacion;
    if(!dni){

      this.error = true;
      this.message = 'Ingrese Numero de documento';
      return;
    }
    
    this.spinner.show();
    this.clearClient();
    this.initialize();
    const body = {
      Filtro : dni,
      IDSede : this.data.IDSede
    };

    this.sclient.getClientBase(body).subscribe( response =>{

      const exito = response.message === 'exito';
      
      if( exito ){
        const result = response.result;
        if( result.length > 0 ){
          
          this.form.patchValue({
            cliente : `${result[0].Nombres} ${result[0].Apellidos}`,
            IDCliente : result[0].IDCliente
          });
        }
        else{
          this.nuevocliente();
        }
      }
      
      this.spinner.hide();
    }, (e) => this.exeception(e) )

  }

  getCertificados(){

    this.spinner.show();
    const body = {
      IDSede  : this.data.IDSede
    };

    this.scert.getCertificadoAutorizado( body).subscribe( response =>{

      this.listCertificados = response;
      this.spinner.hide();
    }, (e) => this.exeception(e) );
  }

  updateCertificado(){

    if(this.form.invalid){
      return this.svalidator.Empty_data(this.form);
    }

    this.spinner.show();
    this.initialize();
    const body = {
      ... this.form.value
    }

    delete body.cliente;
    this.scert.updateCertificado(body).subscribe( response =>{

      const exito = response['message'] === 'exito';
      if(exito){
        this.dialogRef.close({ exito : true });
      }
      this.spinner.hide();
    }, (e) => this.exeception(e) );
  }

  exeception( error:any ){
    
    this.error = true;
    const cnn_expi = error.error === 'Unauthorized';
    this.cnn_expi = cnn_expi;
    this.message = cnn_expi ? 'conexion expirada, vuelva a iniciar sesion' : error.error.message ?? 'Sin conexion al servidor';
    this.spinner.hide();
  }

  initialize() {

    this.success = false;
    this.cnn_expi = false;
    this.error = false;
    this.message = null;
  }

  nuevocliente(){

    const body = {
      Filtro  : this.form.value.NumeroIdentificacion,
      IDSede  : this.data.IDSede,
      reporte : true,
      validarReniec : true
    };

    this.modals.open(NuevoclienteComponent,{ 
      data : body,
      disableClose : true
    }).afterClosed().subscribe( result =>{

      if( result.exito ){

        const operacion = result.nuevo ? "registrado" : 'editado';
        const body = result.body;
        const nombres = `${body.Nombres} ${body.Apellidos}`
        this.form.patchValue({
          cliente : nombres,
          IDCliente : body.IDCliente,
          NumeroIdentificacion : body.NumeroIdentificacion,
          Filtro    : body.NumeroIdentificacion
        });
        this.success = true;
        this.message = `Cliente ${operacion} con éxito!!!`
        this.hiddenExito();
      }

    });
  }

  clearClient(){
    
    this.form.patchValue({
      IDCliente               : '',
      cliente                 : '',
    })
  }

  hiddenExito() {

    setTimeout(() => {
      this.success = false;
    }, 2000);

  }
  
}
