import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditreportecertificadoComponent } from './editreportecertificado.component';

describe('EditreportecertificadoComponent', () => {
  let component: EditreportecertificadoComponent;
  let fixture: ComponentFixture<EditreportecertificadoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditreportecertificadoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditreportecertificadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
