import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-modaldelete',
  templateUrl: './modaldelete.component.html',
  styleUrls: ['./modaldelete.component.css']
})
export class ModaldeleteComponent implements OnInit {

  @Input() error  : boolean = false;
  @Input() message : string = null;
  @Output() Send  = new EventEmitter<boolean>();
  @Output() SendDelete  = new EventEmitter<boolean>();

  constructor() { }
  ngOnInit(): void {}

  _sendError(){

    this.error = !this.error;
    this.Send.emit( this.error);
  }

  _sendDelete( accion:boolean ){

    this.SendDelete.emit( accion  );
    this._sendError();
    
  }

}
