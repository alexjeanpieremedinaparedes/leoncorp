import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModaldeleteComponent } from './modaldelete/modaldelete.component';
import { MaterialModule } from '../material-module';
import { GuadarexitosamenteComponent } from './guadarexitosamente/guadarexitosamente.component';
import { ModalErrorComponent } from './modal-error/modal-error.component';



@NgModule({
  declarations: [
    ModaldeleteComponent,
    GuadarexitosamenteComponent,
    ModalErrorComponent,
  ],

  exports:[
    ModaldeleteComponent,
    GuadarexitosamenteComponent,
    ModalErrorComponent
  ],
  imports: [
    CommonModule, 
    MaterialModule
  ]
})
export class ComponentsModule { }
