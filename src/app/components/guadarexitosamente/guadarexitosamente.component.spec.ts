import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GuadarexitosamenteComponent } from './guadarexitosamente.component';

describe('GuadarexitosamenteComponent', () => {
  let component: GuadarexitosamenteComponent;
  let fixture: ComponentFixture<GuadarexitosamenteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GuadarexitosamenteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GuadarexitosamenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
