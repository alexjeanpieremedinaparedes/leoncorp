import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-guadarexitosamente',
  templateUrl: './guadarexitosamente.component.html',
  styleUrls: ['./guadarexitosamente.component.css']
})
export class GuadarexitosamenteComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  @Input() success  : boolean = false;
  @Input() message  : string;

  alertexito(){
     this.success = !this.success;
  }
}
