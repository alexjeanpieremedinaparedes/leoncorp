import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { IclienteBase, IclientResultBase } from '../interface/client.interface';
import { ValidatorService } from './validator.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  private url:string;
  constructor( private http:HttpClient, private svalidator : ValidatorService ) {
    this.url = environment.endPoint;
  }

  getDepartamento(){
    
    const headers = this.svalidator.headersToken();
    return this.http.post(`${this.url}getDepartamento`, { }, { headers }).pipe( map( result => result['result'] ) );
  }

  getSedes(){

    const headers = this.svalidator.headersToken();
    return this.http.post(`${this.url}getsedeDrop`, { }, { headers }).pipe( map( result => result['result'] ) );
  }

  getTypeDocument(){

    const headers = this.svalidator.headersToken();
    return this.http.post(`${this.url}getTipoDocumentoDrop`, { }, { headers }).pipe( map( result => result['result'] ) );
  }

  getClientesXsede( body:any ) :Observable<IclientResultBase[]> {

    const headers = this.svalidator.headersToken();
    return this.http.post(`${this.url}getclienteDataBase`, body, { headers } ).pipe( map( result => result['result'] ) );
  }

  getclientReniec( body:any ){
    
    const headers = this.svalidator.headersToken();
    return this.http.post(`${this.url}getClienteReniec`, body, { headers })
  }

  getClientBase( body:any ){

    const headers = this.svalidator.headersToken();
    return this.http.post<IclienteBase>(`${this.url}getclienteDataBase`, body, { headers } )
  }

  saveClient( body:any ){

    const headers = this.svalidator.headersToken();
    return this.http.post(`${this.url}createcliente`, body, { headers })
  }

  deleteClient( body:any ){

    const headers = this.svalidator.headersToken();
    const httpOptions = {
      headers: headers,
      body: body
    };
    
    return this.http.delete(`${this.url}deletecliente`, httpOptions);
  }



}
