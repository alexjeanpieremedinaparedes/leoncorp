import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';
import { environment } from 'src/environments/environment';

import *  as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=UTF-8';
const EXCEL_EXT = '.xlsx';

@Injectable({
  providedIn: 'root'
})
export class FunctionService {

  constructor() { }

  exportToExcel(json: any, excelFileName: string): void {

    const workSheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workBook: XLSX.WorkBook = { Sheets: { 'data': workSheet }, SheetNames: ['data'] };
    const excelbuffer: any = XLSX.write(workBook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcel(excelbuffer, excelFileName);

  }

  private saveAsExcel(buffer: any, FileName: string): void {
    const data: Blob = new Blob([buffer], { type: EXCEL_TYPE });
    FileSaver.saveAs(data, FileName + EXCEL_EXT);
  }

  setCookie( name:string, value:string, days:number  ){

    const _date = new Date();
    _date.setTime(_date.getTime() + (days*24*60*60*1000) );
    const _expires = "; expires=" + _date.toUTCString();

    document.cookie = `${name}=${value}${_expires};path=/`;
  }

  getCookie( name:string ){
    
    let _name = escape(name);
    const _allCookies = document.cookie;

    _name += "=";
    const pos = _allCookies.indexOf(_name);

    if(pos != -1){

      const start = pos + _name.length;
      let end = _allCookies.indexOf(';', start);
      if(end == -1) end = _allCookies.length;
      const value = _allCookies.substring(start, end);
      return value;
    }

    return '';
  }

  deleteCookie( name:string ){
    const _name = escape(name);
    const _expires = new Date(0);

    document.cookie = _name + "="+ ";expires=" + _expires.toUTCString() + ";path=/";

  }

  encrypt( text:string ){
    
    return CryptoJS.AES.encrypt(text.trim(), environment.clave).toString();
  }

  decrypt( text:string ){

    return CryptoJS.AES.decrypt(text.trim(), environment.clave).toString( CryptoJS.enc.Utf8 );
  }

  convertbs64ForSrc(bs64img): string {
    return 'data:image/jpeg;base64,' + bs64img;
  }

  search(array: any[], column : string, text: string): any[] {

    if (!text) return array;
    return array.filter(item => String(item[column]).toUpperCase().includes(text.toUpperCase()));
  }

  fecha_actual(): string {
    let fecha: string;
    const dat = new Date();
    const dia = dat.getDate();
    const mes = dat.getMonth() + 1;
    const año = dat.getFullYear();

    if (mes < 10) fecha = `${año}-0${mes}-${dia}`;
    else fecha = `${año}-${mes}-${dia}`;
    return fecha;
  }

  convert_fecha(fecha: string) {

    const dat = new Date(fecha);
    const dia = dat.getDate();
    const mes = dat.getMonth() + 1;
    const año = dat.getFullYear();

    if (mes < 10) fecha = `${año}-0${mes}-${dia}`;
    else fecha = `${año}-${mes}-${dia}`;
    return fecha;
  }

  printDocument( bs64File : any ){

    let byteCharacters = atob(bs64File);
    let byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    let byteArray = new Uint8Array(byteNumbers);
    let file = new Blob([byteArray], {type: 'application/pdf;base64'});
    let fileURL = URL.createObjectURL(file)
    const iframe = document.createElement('iframe');
    iframe.style.display = 'none';
    iframe.src = fileURL;
    document.body.appendChild(iframe);
    iframe.contentWindow.print();

  }

}