import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ICertificado, ICertReporte } from '../interface/certificado.interface';
import { ValidatorService } from './validator.service';

@Injectable({
  providedIn: 'root'
})
export class CertificadoService {
  private url:string;
  constructor( private http:HttpClient, private svalidator : ValidatorService ) {
    this.url = environment.endPoint;
  }

  getCertificadoAutorizado( body:any ) : Observable<ICertificado[]>{
    const headers = this.svalidator.headersToken();
    return this.http.post<ICertificado[]>(`${this.url}getCertificadoAutorizado`, body , { headers }).pipe( map( result => result['result'] ) );
  }

  SaveCertificado( body: any ){
    const headers = this.svalidator.headersToken();
    return this.http.post(`${this.url}createCertificado`, body , { headers });
  }

  getReporteCertificado( body : any ) : Observable<ICertReporte[]>{

    const headers = this.svalidator.headersToken();
    return this.http.post(`${this.url}getCertificado`, body, { headers } ).pipe( map( result => result['result'] ) );
  }

  updateCertificado( body : any ){
    const headers = this.svalidator.headersToken();
    return this.http.put(`${this.url}updateCertificado`, body, { headers } );
  }

  printCertificado( body:any ){
    const headers = this.svalidator.headersToken();
    return this.http.post(`${this.url}printCertificado`, body, { headers });
  }

  printCertificadoGrupal( body:any ){
    const headers = this.svalidator.headersToken();
    return this.http.post(`${this.url}getCertificados`, body, { headers });
  }
}
