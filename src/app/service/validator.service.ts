import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ValidatorService {

  constructor() { }

  Empty_data( form : FormGroup ){

    return Object.values(form.controls).forEach(control => {
      if (control instanceof FormGroup) {
        Object.values(control.controls).forEach(control => control.markAsTouched());
      }
      else {
        control.markAsTouched();
      }
    });
  }

  control_invalid( campo:string, form : FormGroup ){
    return form.get(campo).invalid && form.get(campo).touched;
  }

  addValidator( campo:string, required:any, form:FormGroup ){
    form.controls[campo].setValidators(required);
    form.controls[campo].updateValueAndValidity();
  }

  validateKey(event, limite) : boolean {

    let oldval = event.target.value;
    const val = oldval.slice(0,event.target.selectionStart) + event.key + oldval.slice(event.target.selectionEnd);

    if( val.length > limite ) return false;
    return true;
  }

  SoloNumeros( $event ){

    const KEY_ALPHABET = [ 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90 ];

    if( KEY_ALPHABET.includes($event.keyCode) ){
      return false;
    }

    return true;

  }

  headersToken(): any {
    const token = JSON.parse(localStorage.getItem('_loginleon')).token;
    return { 'Authorization': `bearer ${token}` }
  }

}
