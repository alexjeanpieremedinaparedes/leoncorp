import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { IAuth } from '../interface/auth.interface';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private url:string;
  constructor( private http:HttpClient, private router:Router ) {
    this.url = environment.endPoint;
  }

  login( body:any ){

    return this.http.post<IAuth>(`${this.url}login`, body)
  }

  logout(){
    localStorage.removeItem('_loginleon');
    this.router.navigateByUrl('/login');
  }
}
