import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImageInvalidDirective } from './image-invalid.directive';



@NgModule({
  declarations: [
    ImageInvalidDirective
  ],
  exports:[
    ImageInvalidDirective
  ],
  imports: [
    CommonModule
  ]
})
export class DirectiveModule { }
