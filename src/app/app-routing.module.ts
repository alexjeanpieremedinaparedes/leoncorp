import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { DashboardRoutingModule } from './pages/dashboard/dashboard.routing';

const routes: Routes = [

  { path: 'login', component: LoginComponent },
  { path: '**', redirectTo: 'login', pathMatch: 'full' },

 

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true }), 
    DashboardRoutingModule 
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
