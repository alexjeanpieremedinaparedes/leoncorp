import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuthService } from 'src/app/service/auth.service';
import { FunctionService } from 'src/app/service/function.service';
import { ValidatorService } from 'src/app/service/validator.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form     : FormGroup;
  remember : boolean;
  error    : boolean;
  message  : string;

  constructor(
    private sauth      : AuthService,
    public svalidator  : ValidatorService,
    private sfn        : FunctionService,
    private router     : Router,
    private fb         : FormBuilder,
    private spinner    : NgxSpinnerService,
    ) {
    
    const passDecrypt = this.sfn.decrypt(this.sfn.getCookie('pass'));
    this.remember = JSON.parse(localStorage.getItem('remember')) ?? false;
    const dni = this.remember ? this.sfn.getCookie('dni') : '';
    const pass = this.remember ? passDecrypt : '';

    this.create_form(dni, pass);
  }

  ngOnInit(): void {

    const userLoggedin = localStorage.getItem('_loginleon');
    if(userLoggedin) this.navigateRouter();

  }

  get dniInvalid(){
    return this.svalidator.control_invalid('dni', this.form);
  }

  get passInvalid(){
    return this.svalidator.control_invalid('password', this.form);
  }

  create_form( dni:string, pass:string ){

    this.form = this.fb.group({
      dni      : [ dni, [ Validators.required, Validators.pattern('^[0-9]{8}$') ] ],
      password : [ pass, [ Validators.required ] ],
      remember : [ this.remember, [ Validators.required ] ]
    });
  }

  login(){

    if(this.form.invalid){
      return this.svalidator.Empty_data(this.form);
    }

    this.initialize();
    const body = {
      ... this.form.value
    };

    delete body.remember;
    this.sauth.login(body).subscribe( response =>{

      const exito = response.message === 'usuario autenticado';
      if( exito ){

        const result = response.result;
        localStorage.setItem('_loginleon', JSON.stringify(result) );
        this.rememberLogin();
        this.navigateRouter();
      }
      else this.sauth.logout();
      this.spinner.hide();
    }, (e) => this.exception(e) );
  }

  rememberLogin(){

    const remember = this.form.value.remember;
    if( remember ){

      const dni = this.form.value.dni;
      const pass = this.sfn.encrypt(this.form.value.password);

      this.sfn.setCookie('dni', dni, 360 );
      this.sfn.setCookie('pass', pass, 360 );
      localStorage.setItem('remember', remember);
    }
    else{

      this.sfn.deleteCookie('dni');
      this.sfn.deleteCookie('pass');
      localStorage.removeItem('remember');
    }

  }

  navigateRouter(){
    this.router.navigateByUrl('/dashboard');
  }

  initialize(){
    this.spinner.show();
    this.error = false;
    this.message = null;
  }

  exception(e:any){

    this.sauth.logout();
    this.error = true;
    this.message = e.error.message ?? "Sin conexion al servidor";
    this.spinner.hide();
  }

}
