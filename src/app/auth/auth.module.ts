import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { RouterModule } from '@angular/router';
import { NgxSpinnerModule } from 'ngx-spinner';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [LoginComponent],
  
  imports: [
    CommonModule,
    RouterModule,
    NgxSpinnerModule,
    ReactiveFormsModule
  ]
})
export class AuthModule { }
