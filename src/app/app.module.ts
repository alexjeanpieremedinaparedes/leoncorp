import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import LocaleEsPe from '@angular/common/locales/es-PE.js';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material-module';
import { NgxSpinnerModule } from 'ngx-spinner';
import { DashboardModule } from './pages/dashboard/dashboard.module';
import { AuthModule } from './auth/auth.module';
import { ComponentsModule } from './components/components.module';
import { EditreportecertificadoComponent } from './pages/modal/editreportecertificado/editreportecertificado.component';
import { HttpClientModule } from '@angular/common/http';
import { registerLocaleData } from '@angular/common';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { ReactiveFormsModule } from '@angular/forms';
import { VerCertificadoComponent } from './pages/modal/ver-certificado/ver-certificado.component';
import { RegistrarnuevocliComponent } from './pages/modal/registrarnuevocli/registrarnuevocli.component';

registerLocaleData(LocaleEsPe);

@NgModule({
  declarations: [
    AppComponent,
    EditreportecertificadoComponent,
    VerCertificadoComponent,
    RegistrarnuevocliComponent
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,  
    NgxSpinnerModule, 
    AuthModule,
    DashboardModule, 
    ComponentsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],

  providers: [
    { 
      provide: MAT_DATE_LOCALE,
      useValue: 'es-PE'
    },
    {
      provide: LOCALE_ID,
      useValue: 'es-PE'
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
