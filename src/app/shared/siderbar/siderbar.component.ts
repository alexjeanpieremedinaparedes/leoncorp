import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-siderbar',
  templateUrl: './siderbar.component.html',
  styleUrls: ['./siderbar.component.css']
})
export class SiderbarComponent implements OnInit {


  ngOnInit(): void {
  }

  panelOpenState = false;


  openTab1: boolean = false;
  openTab2: boolean = false;
  openTab3: boolean = false;
  openTab4: boolean = false;
  openTab5: boolean = false;
  openTab6: boolean = false;

  changetabs() {
    this.openTab1 = false;
    this.openTab2 = false;
    this.openTab3 = false;
  }

  toggleTabs($tabNumber: number) {
    this.changetabs();
    if ($tabNumber === 1) this.openTab1 = true;
    else if ($tabNumber == 2) this.openTab2 = true;
    else if ($tabNumber == 3) this.openTab3 = true;
  }

  constructor(private router: Router) {
    const ruta = this.router.url;
    if (ruta.includes('/dashboard/registrocertificado')) this.openTab1 = true;
    else if (ruta.includes('/dashboard/reportecertificado')) this.openTab2 = true;
    else if (ruta.includes('/dashboard/clientes')) this.openTab3 = true;
  }

}
