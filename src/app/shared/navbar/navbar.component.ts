import { Component, OnInit } from '@angular/core';
import { AuthResult } from 'src/app/interface/auth.interface';
import { AuthService } from 'src/app/service/auth.service';
import { FunctionService } from 'src/app/service/function.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  data = {} as AuthResult;
  constructor( public sauth:AuthService, private sfn:FunctionService ) {

    const _loginleon = localStorage.getItem('_loginleon');
    if(_loginleon){

      const info = JSON.parse(_loginleon);
      this.data.Nombre = info.Nombre;
      this.data.RazonSocial = info.RazonSocial;
      this.data.Administrador = info.Administrador;
      this.data.Foto = this.sfn.convertbs64ForSrc(info.Foto);
    }

  }

  ngOnInit(): void {
  }

}
