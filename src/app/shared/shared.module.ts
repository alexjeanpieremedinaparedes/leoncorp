import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { SiderbarComponent } from './siderbar/siderbar.component';
import { MaterialModule } from '../material-module';
import { RouterModule } from '@angular/router';
import { DirectiveModule } from '../directive/directive.module';



@NgModule({
  declarations: [
    NavbarComponent, 
    SiderbarComponent
  ],

  exports: [
    NavbarComponent, 
    SiderbarComponent
  ],
  imports: [
    CommonModule,
     MaterialModule,
     RouterModule,
     DirectiveModule
  ]
})
export class SharedModule { }
