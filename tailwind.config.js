const { guessProductionMode } = require("@ngneat/tailwind");

process.env.TAILWIND_MODE = guessProductionMode() ? 'build' : 'watch';

module.exports = {
    prefix: '',
    mode: 'jit',
    purge: {
      content: [
        './src/**/*.{html,ts,css,scss,sass,less,styl}',
      ]
    },
    darkMode: 'class', // or 'media' or 'class'
    theme: {
      extend: {
        colors: {
          'amarillo': {
              '500': '#F8C611',
              '600': '#E7C126'
          },
          'plomo': {
              '100': '#F0F0F0',
              '150': '#F5F3F1',
              '180': '#ECECEC',
              '200': '#F6F6F6',
              '300': '#BCBCBC',
              '330': '#959595',
              '400': '#707070',
              '450': '#7A7A7A',
              '500': '#3E3E3E',
              '900': '#1A1A1A', 
              '21': '#4C546C'
          }
      },

      },
    },
    variants: {
      extend: {},
    },
    plugins: [require('@tailwindcss/aspect-ratio'),require('@tailwindcss/forms'),require('@tailwindcss/line-clamp'),require('@tailwindcss/typography')],
};
